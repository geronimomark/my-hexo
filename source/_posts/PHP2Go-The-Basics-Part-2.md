---
title: PHP2Go - The Basics Part 2
date: 2016-10-25 06:08:46
tags:
 - PHP
 - Go
categories:
 - Tech
comments: true
---
## Continuation of PHP2Go - The Basics.

On our last {% link post http://markg.ninja:8080/blog/2016/10/24/PHP2Go-The-Basics-Part-1/ %}, we tackled the classic "Hello world" and variable declarations in PHP and Go. Today we will continue the basics, this time with conditional and switch statements.

Like our initial post, this would be like a cheat-sheet of the equivalent syntax of PHP to Go. So, let's get it on.

If statement
{% codeblock lang:php %}
<?php
    $year = date("Year");
    if ($year == 2016) {
        echo $year;
    }
?>
{% endcodeblock %}
{% codeblock lang:go %}
package main
import (
    "fmt"
    "time"
)
func main() {
    year := time.Now().Year()
    if year == 2016 {
        fmt.Println(year)
    }
}
{% endcodeblock %}

If Else statement
{% codeblock lang:php %}
<?php
    $year = date("Year");
    if ($year == 2016) {
        echo $year;
    } else {
        echo "I miss year 2016";
    }
?>
{% endcodeblock %}
{% codeblock lang:go %}
package main
import (
    "fmt"
    "time"
)
func main() {
    year := time.Now().Year()
    if year == 2016 {
        fmt.Println(year)
    } else {
        fmt.Println("I miss year 2016")
    }
}
{% endcodeblock %}

If ElseIf Else statement
{% codeblock lang:php %}
<?php
    $year = date("Year");
    if ($year < 2016) {
        echo "I can't wait for 2016!";
    } elseif ($year >= 2016 && $year <= 2017) {
        echo "This is the best years of our lives!"
    } else {
        echo "I miss year 2016 and 2017";
    }
?>
{% endcodeblock %}
{% codeblock lang:go %}
package main
import (
    "fmt"
    "time"
)
func main() {
    year := time.Now().Year()
    if year < 2016 {
        fmt.Println("I can't wait for 2016!")
    } else if year >= 2016 && year <= 2017 {
        fmt.Println("This is the best years of our lives!")
    } else {
        fmt.Println("I miss year 2016 and 2017")
    }
}
{% endcodeblock %}

Switch statement
{% codeblock lang:php %}
<?php
    $year = date("Year");
    switch ($year) {
        case year < 2016:
            echo "I can't wait for 2016!";
            break;
        case 2016:
            echo "This is the best years of our lives!";
            break;
        default:
            echo "I miss year 2016 and 2017";
    }
?>
{% endcodeblock %}
{% codeblock lang:go %}
package main
import (
    "fmt"
    "time"
)
func main() {
    year := time.Now().Year()
    switch year {
        case year < 2016:
            fmt.Println("I can't wait for 2016!")
            break
        case 2016:
            fmt.Println("This is the best years of our lives!")
            break
        default:
            fmt.Println("I miss year 2016 and 2017")
    }
}
{% endcodeblock %}