---
title: Git Cheat-Sheet
date: 2016-10-26 15:26:17
tags:
 - Git
categories:
 - Tech
comments: true
---
## Git Cheat-Sheet

{% link Git https://git-scm.com/ %} has been a powerful tool that every developer should learn. Although most of the code editors right now already have a plugin for instant "push" or "pull" on a repository, it doesnt hurt to know and master the git commands.

For this post, we will share our Git cheat-sheet which is commonly used on our day-to-day software development. Have fun!

Creating a local repository
{% codeblock %}
# git init
initialize a local repository on current directory
# git clone https://github.com/geronimomark/repo.git
clone an existing repository. we can clone either thru https or ssh
{% endcodeblock %}

Tracking Local Repository Changes
{% codeblock %}
# git status
shows changed files in current workspace
# git diff
show changes on all changed files
# git diff filename.php anotherfile.php
show changes on specified files
# git add .
stage all changed files for commit
# git add filename.php
stage specific file for commit
# git rm filename.php
delete a file in repository
# git commit -m "Add new function"
commit all staged items with message
{% endcodeblock %}

Viewing Commit History
{% codeblock %}
# git log
show all commits, order by newest
# git show 9bb0cd2f8cca4fadc84ec8eee4bff33f8f0e2248
show all changes in every file on a specific commit hash code (hash can be found on git log command)
# git log -p file.php
show all commits on a specific file
# git blame file.php
show full history of every line of code, including date and name of user
{% endcodeblock %}

Creating Branches and Tags
{% codeblock %}
# git branch -a
list all local and remote-tracking branches
# git checkout existing-branch-or-tag
switch to existing branch or tag
# git checkout -b new-branch
create and switch to a new branch
# git branch -d branch-to-delete
delete a local branch
# git tag my-tag-name
mark current commit with a tag
{% endcodeblock %}

Updating and Publishing
{% codeblock %}
# git remote -v
show all remote name and url
# git remote add my-new-remote https://github.com/geronimomark/my-repo.git
add a new remote repository
# git pull my-remote my-branch
fetch and merge all remote changes to local repository
# git push my-remote my-branch
publish local changes to remote repository
# git push --tags
publish tags to remote repository
{% endcodeblock %}

Merging and Rebase
{% codeblock %}
# git merge another-branch
merge another branch to current branch
# git rebase other-branch
rebase current HEAD to other-branch
# git rebase --abort
abort previous rebase
{% endcodeblock %}

Undo changes
{% codeblock %}
# git reset --hard HEAD
discard all local changes
# git checkout HEAD file.php
discard changes on specific file
# git reset --hard 9bb0cd2f8cca4fadc84ec8eee4bff33f8f0e2248
reset HEAD pointer to a previous commit (use with caution, will discard commits that are newer than the commit hash)
{% endcodeblock %}
