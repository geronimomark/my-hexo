---
title: Go - Creating and styling excel files
tags:
  - Go
categories:
  - Tech
comments: true
date: 2017-02-19 15:11:59
---


### Working with excel files in Go

Creating excel files has been easy for every languages I've used before (PHP, C#, Java) due to the fact that they are old languages and have great documentation. With Go, utilizing a 3rd party package is a no-brainer and a smart choice. However, the documentation is usually poor so I decided to post this to be handful for other searching the same information.

In this post, we will use {% link tealeg/xlsx https://github.com/tealeg/xlsx %} package.

First, we will set our font style
{% codeblock lang:go %}
package main

import (
	"github.com/tealeg/xlsx"
)

func main() {
	var file *xlsx.File
	var sheet *xlsx.Sheet
	var row *xlsx.Row
	var cell *xlsx.Cell
	var err error
	
	// Set Font
	font := *xlsx.NewFont(10, "Arial")
	font.Underline = true
	font.Bold = true
}	
{% endcodeblock %}

Next is setting up our border thickness
{% codeblock lang:go %}
func main() {
    	// initial codes here 
    	// ...

  	// Set Border
	border := *xlsx.NewBorder("thin", "thin", "thin", "thin")    
}
{% endcodeblock %}

Next is our styles for our background color
{% codeblock lang:go %}
func main() {
    	// initial codes here 
    	// ...

        // Set Fill
	fill := *xlsx.NewFill("solid", "FFFF00", "000000")
}
{% endcodeblock %}

Now we can apply it in our styles object
{% codeblock lang:go %}
func main() {
    	// initial codes here 
    	// ...

        // Set style
	style := xlsx.NewStyle()
	style.Font = font
	style.Border = border
	style.Fill = fill
	style.ApplyFont = true
	style.ApplyBorder = true
	style.ApplyFill = true
}
{% endcodeblock %}

Finally, we create our excel file
{% codeblock lang:go %}
func main() {
    	// initial codes here 
    	// ...

	file = xlsx.NewFile()
	sheet, err = file.AddSheet("Sheet 1")
	if err != nil {
		log.Println(err.Error())
	}

	row = sheet.AddRow()
	cell = row.AddCell()
	cell.SetStyle(style)
	cell.SetValue("Here we go!")

	err = file.Save("MyExcelFile.xlsx")
	if err != nil {
		log.Println(err.Error())
	}
}
{% endcodeblock %}

Once we run our Go program, the excel file will be created in our root folder. Nice!


