---
title: PSR What??
date: 2016-10-30 12:46:22
tags:
 - PHP
categories:
 - Tech
comments: true
---
## PSR for noobs

What is {% link PSR http://www.php-fig.org/psr/ %}? PSR stands for PHP Standards Recommendations. It was established by PHP developers with a sole purpose of having a standard in everything about PHP software development. So should I follow this standards? Is it required? Not really, you can do your own hack and slash style whatever stuff that you do, but including PSR in your software development practice is a big plus in your portfolio.

In this post, we will go through the accepted PSR-1 and 2 (Coding standards). We will not go into details but rather provide tips and directions on where to go from here.

PSR-1 and 2 (Coding Standards)
- Files MUST use only <?php and <?= tags. Other tag variations such as ASP and script tags are removed in PHP 7.
- Files MUST use only UTF-8 without BOM for PHP code. BOM causes errors when parsing PHP files. (TIP: set your code editor to be UTF-8)
- Files SHOULD either declare symbols (classes, functions, constants, etc.) or cause side-effects (e.g. generate output, change .ini settings, etc.) but SHOULD NOT do both. This rules main focus is to segregate PHP files accordingly, more of an MVC ideology and best practice
- Namespaces and classes MUST follow an "autoloading". This rule requires an understanding of OOP and PSR-0 or PSR-4
- Class names MUST be declared in StudlyCaps. This rule eliminates the wide usage of Snake_Case declarations in PHP which is very popular in Codeigniter and Zend Frameworks
- Class constants MUST be declared in all upper case with underscore separators. 
- Method names MUST be declared in camelCase. This rule also moves away the snake_case function declaration
- Code MUST use 4 spaces for indenting, not tabs. RIP tabs
- There MUST NOT be a hard limit on line length; the soft limit MUST be 120 characters; lines SHOULD be 80 characters or less. This rule is one of my favorites and this aim to eliminate long lines of single line code.
- There MUST be one blank line after the namespace declaration, and there MUST be one blank line after the block of use declarations. This rule is more on aesthetics
- Opening braces for classes MUST go on the next line, and closing braces MUST go on the next line after the body. This rule is more on aesthetics
- Opening braces for methods MUST go on the next line, and closing braces MUST go on the next line after the body. This rule is more on aesthetics
- Visibility MUST be declared on all properties and methods; abstract and final MUST be declared before the visibility; static MUST be declared after the visibility. (TIP: Functions in PHP are always public when not defined.)
- Control structure keywords MUST have one space after them; method and function calls MUST NOT. This rule is more on aesthetics
- Opening braces for control structures MUST go on the same line, and closing braces MUST go on the next line after the body. This rule is more on aesthetics
- Opening parentheses for control structures MUST NOT have a space after them, and closing parentheses for control structures MUST NOT have a space before. This rule is more on aesthetics
