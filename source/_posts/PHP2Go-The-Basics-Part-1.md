---
title: PHP2Go - The Basics Part 1
date: 2016-10-24 11:15:06
tags:
 - PHP
 - Go
categories:
 - Tech
comments: true
---
## A simple guide for developers from PHP to Golang.

A basic introduction first for PHP. According to www.w3schools.com, PHP is a server scripting language, and a powerful tool for making dynamic and interactive Web pages. PHP is a widely-used, free, and efficient alternative to competitors such as Microsoft's ASP. Great! If you are after a tutorial for PHP, simply follow this link http://www.w3schools.com/php/.

Alright, now what is Go? Is this the opposite of Stop? Just kidding. Go is an open source programming language that makes it easy to build simple, reliable, and efficient software. Find out more by going to https://golang.org/.

This post assumes you already have the necessary services, packages, etc.. needed to run PHP and Go. If you don't have those, do some googling to find out :D.

We will start with the classic "Hello World" example.
{% codeblock lang:php %}
<?php
    echo "Hello World!";
?>
{% endcodeblock %}
{% codeblock lang:go %}
package main
import "fmt"

func main() {
    fmt.Print("Hello World!")
}
{% endcodeblock %}

Easy right? Now lets declare variables...
{% codeblock lang:php %}
<?php
    $string = "Mark";
    $number = 23;
    $float  = 99.99;
    $array1 = [1,2,3];
    $array2 = ["name" => "Mark", "gender" => "M"];
?>
{% endcodeblock %}
{% codeblock lang:go %}
package main
import "fmt"

func main() {
    // Short-cut
    string1 := "Mark"
    number1 := 23
    float1  := 99.99
    array1  := []int{1, 2, 3}
    maps1   := map[string]interface{
        "name" : "Mark",
        "age" : 30,
    }

    // The Long and winding road
    var string2 string
    string2 = "Mark"
    var number2 int
    number2 = 23
    var float2 float32
    float2 = 99.99
    var array2 []int
    array2 = append(array2, 1)
    array2 = append(array2, 2)
    array2 = append(array2, 3)
    var maps2 map[string]interface
    maps2["name"] = "Mark"
    maps2["age"] = 30

    fmt.Println(string1, string2)
    fmt.Println(number1, number2)
    fmt.Println(float1, float2)
    fmt.Println(array1, array2)
    fmt.Println(maps1, maps2)
}
{% endcodeblock %}

As you can see, PHP and Go's short-hand syntax is fairly similar, minus the semicolons and data type declarations on arrays and maps. Go is very strict on data types unlike in PHP. So observe, learn and deal with it. Also, you should always use a declared variable in Go, otherwise, it will throw an error.
